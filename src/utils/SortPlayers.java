package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import model.Player;

public class SortPlayers {
	
    private SortPlayers() {
    }

    public static void ordenarPorTodo(ArrayList<Player> jugadores) {
        Collections.sort(jugadores, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                if (o2.getPuntos() - o1.getPuntos() == 0) {
                	 if (o1.getPartidasJugadas() - o2.getPartidasJugadas() == 0) {
                         if (o1.getName().compareTo(o2.getName()) == 0) {
                             return o1.getName().length() - o2.getName().length();
                         } else {
                             return o1.getName().compareTo(o2.getName());
                         }
                     } else {
                         return o1.getPartidasJugadas() - o2.getPartidasJugadas();
                     }
                } else {
                    return o2.getPuntos() - o1.getPuntos();
                }
            }
        });
    }
}
