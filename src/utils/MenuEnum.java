package utils;

public enum MenuEnum {

	JUGAR("Jugar", 1), ANNADIR("A�adir", 2),  INSTRUCCIONES("Instrucciones", 3), RANKING("Ranking", 4), SALIR("Salir", 5);

	private final String OPCION_NOMBRE_MENU;
	private final int OPCION_VALOR_MENU;

	private MenuEnum(String OPCION_NOMBRE_MENU, int OPCION_VALOR_MENU) {
		this.OPCION_NOMBRE_MENU = OPCION_NOMBRE_MENU;
		this.OPCION_VALOR_MENU = OPCION_VALOR_MENU;
	}

	public String getOPCION_NOMBRE_MENU() {
		return OPCION_NOMBRE_MENU;
	}

	public int getOPCION_VALOR_MENU() {
		return OPCION_VALOR_MENU;
	}

}
