package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import main.Main;
import model.Question;

public class ReadQuestionsXML {

	private ReadQuestionsXML() {

	}

	public static void leer(ArrayList<Question> questions, String ruta) {

		SAXBuilder builder = new SAXBuilder();

		File xmlFile = new File(ruta);

		if (xmlFile.exists()) {
			try {
				Document document = (Document) builder.build(xmlFile);

				// Base
				Element rootNode = document.getRootElement();

				List<Element> tarjeta = rootNode.getChildren("tarjeta");

				for (int i = 0; i < tarjeta.size(); i++) {
					Element nodo = tarjeta.get(i);

					String nombrePregunta = nodo.getChildText("pregunta");
					String sol1 = nodo.getChildText("sol1");
					String sol2 = nodo.getChildText("sol2");
					String sol3 = nodo.getChildText("sol3");
					String[] soluciones = { sol1, sol2, sol3 };
					String dificultad = nodo.getChildText("dificultad");
					String correcta = nodo.getChildText("correcta");
					int x = 0;
					boolean encontradoDificultad = false;
					while (x < Main.dificultadArray.length && !encontradoDificultad) {
						if (dificultad.equals(Main.dificultadArray[x])) {
							encontradoDificultad = true;
						}
						if (dificultad.equalsIgnoreCase(Main.dificultadArray[x])) {
							dificultad = dificultad.toLowerCase();
							encontradoDificultad = true;
						}
						x++;
					}
					if (!encontradoDificultad) {
						System.out.println("Error al leer "+ruta+": los datos de dificultad de la tarjeta " + (i + 1)
								+ " son incorrectos.");
						System.exit(0);
					}

					int j = 0;
					boolean encontrado = false;
					while (!encontrado && j < soluciones.length) {
						if (correcta.equalsIgnoreCase(soluciones[j])) {
							encontrado = true;
						}
						j++;
					}
					if (!encontrado) {
						System.out.println("Error al leer "+ruta+": los datos de las soluciones de la tarjeta " + (i + 1)
								+ " son incorrectos.");
						System.exit(0);
					}
					questions.add(new Question(nombrePregunta, soluciones, soluciones[j - 1], dificultad));
				}
			} catch (Exception io) {
				io.printStackTrace();
				System.out.println(io.getMessage());
			}
		}
		// else {
		// System.out.println("Fichero de preguntas no encontrado...");
		// }
	}
}
