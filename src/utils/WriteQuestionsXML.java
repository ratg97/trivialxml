package utils;

import java.io.FileOutputStream;
import java.io.FileWriter;

import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;

import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import model.Question;

public class WriteQuestionsXML {

	public static void escribir(ArrayList<Question> questions, String ruta) {

		try {

			Element trivial = new Element("trivial");
			Document doc = new Document(trivial);

			for (int i = 0; i < questions.size(); i++) {

				Element pregunta = new Element("tarjeta");

				Element pregunta_nombre = new Element("pregunta").setText(questions.get(i).getPregunta());
				pregunta.addContent(pregunta_nombre);

				Element pregunta_dificultad = new Element("dificultad").setText(questions.get(i).getDificultad());
				pregunta.addContent(pregunta_dificultad);

				Element sol1 = new Element("sol1").setText(questions.get(i).getSoluciones()[0]);
				Element sol2 = new Element("sol2").setText(questions.get(i).getSoluciones()[1]);
				Element sol3 = new Element("sol3").setText(questions.get(i).getSoluciones()[2]);

				pregunta.addContent(sol1);
				pregunta.addContent(sol2);
				pregunta.addContent(sol3);

				Element correcta = new Element("correcta").setText(questions.get(i).getCorrecta());
				pregunta.addContent(correcta);

				doc.getRootElement().addContent(pregunta);
			}

			// Salida
			FileOutputStream fos = new FileOutputStream(ruta);
            Format format = Format.getPrettyFormat();
            format.setEncoding("UTF-8");
            XMLOutputter outputter = new XMLOutputter(format);
            outputter.output(doc, fos);

			// System.out.println("Fichero Guardado! en "+ruta);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
