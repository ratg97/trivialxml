package utils;

public enum ImportEnum {

	BUSCAR("Buscar fichero", 1), ANNADIR("A�adir preguntas", 2), ATRAS("Atr�s",3);

	private final String OPCION_NOMBRE_IMPORTAR;
	private final int OPCION_VALOR_IMPORTAR;
	
	private ImportEnum(String oPCION_NOMBRE_IMPORTAR, int oPCION_VALOR_IMPORTAR) {
		OPCION_NOMBRE_IMPORTAR = oPCION_NOMBRE_IMPORTAR;
		OPCION_VALOR_IMPORTAR = oPCION_VALOR_IMPORTAR;
	}

	public String getOPCION_NOMBRE_IMPORTAR() {
		return OPCION_NOMBRE_IMPORTAR;
	}

	public int getOPCION_VALOR_IMPORTAR() {
		return OPCION_VALOR_IMPORTAR;
	}
}