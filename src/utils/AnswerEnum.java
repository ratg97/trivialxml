package utils;

public enum AnswerEnum {

	SI("Si", 1), NO("No", 0);

	private final String OPCION_NOMBRE_RESPUESTA;
	private final int OPCION_VALOR_RESPUESTA;

	private AnswerEnum(String oPCION_NOMBRE_RESPUESTA, int oPCION_VALOR_RESPUESTA) {
		OPCION_NOMBRE_RESPUESTA = oPCION_NOMBRE_RESPUESTA;
		OPCION_VALOR_RESPUESTA = oPCION_VALOR_RESPUESTA;
	}

	public String getOPCION_NOMBRE_RESPUESTA() {
		return OPCION_NOMBRE_RESPUESTA;
	}

	public int getOPCION_VALOR_RESPUESTA() {
		return OPCION_VALOR_RESPUESTA;
	}
}
