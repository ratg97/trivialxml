package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import model.Player;

public class ReadPlayersXML {
	private ReadPlayersXML() {

	}

	public static void leerJugador(ArrayList<Player> jugadores, String ruta) {

		SAXBuilder builder = new SAXBuilder();

		File xmlFile = new File(ruta);

		if (xmlFile.exists()) {
			try {
				Document document = (Document) builder.build(xmlFile);

				// Base
				Element rootNode = document.getRootElement();

				List<Element> tarjeta = rootNode.getChildren("jugador");

				for (int i = 0; i < tarjeta.size(); i++) {
					Element nodo = tarjeta.get(i);

					String nombreJ = nodo.getChildText("nombre");
					String puntos = nodo.getChildText("puntos");
					String partidasJugadas = nodo.getChildText("partidasJugadas");
					// As� evitamos que desde el xml se a�adan jugadores dobles
					if (!jugadores.isEmpty() && leerJugadorNombre(jugadores, nombreJ)) {
						System.out.println("Error al leer " + ruta + ": el nombre de jugador " + (i + 1)
								+ " ya existe.. y solo puede haber un nick IGUAL");
						System.exit(0);
					}
					jugadores.add(new Player(nombreJ, Integer.parseInt(puntos), Integer.parseInt(partidasJugadas)));
				}
			} catch (Exception io) {
				io.printStackTrace();
				System.out.println(io.getMessage());
			}
		} else {
			System.out.println("Fichero de jugadores no encontrado...");
		}
	}

	public static boolean leerJugadorNombre(ArrayList<Player> jugadores, String nick) {

		for (int i = 0; i < jugadores.size(); i++) {
			if (jugadores.get(i).getName().equalsIgnoreCase(nick)) {
				return true;
			}
		}
		return false;
	}

	public static Player obtenerJugador(ArrayList<Player> jugadores, String nick) {
		for (int i = 0; i < jugadores.size(); i++) {
			if (jugadores.get(i).getName().equalsIgnoreCase(nick)) {
				return jugadores.get(i);
			}
		}
		return null;
	}

	public static void modificarJugador(ArrayList<Player> jugadores, Player j, int puntos) {
		ArrayList<Player> jugadoresCopia = new ArrayList<Player>();
		for (int i = 0; i < jugadores.size(); i++) {
			if (jugadores.get(i).getName().equalsIgnoreCase(j.getName())) {
				jugadores.get(i).setPuntos(jugadores.get(i).getPuntos() + puntos);
				jugadores.get(i).setPartidasJugadas(jugadores.get(i).getPartidasJugadas() + 1);
			}
			jugadoresCopia.add(jugadores.get(i));
		}

		jugadores.clear();

		for (int i = 0; i < jugadoresCopia.size(); i++) {
			jugadores.add(jugadoresCopia.get(i));
		}

		jugadoresCopia.clear();
		// System.out.println("\nDatos actualizados :)");
	}
}
