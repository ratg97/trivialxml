package utils;

import java.io.FileWriter;
import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import model.Player;

public class WritePlayersXML {
	private WritePlayersXML() {
	}

	public static void escribirJugadores(ArrayList<Player> jugadores, String ruta) {

		try {

			Element jugadoresNodo = new Element("jugadores");
			Document doc = new Document(jugadoresNodo);

			for (int i = 0; i < jugadores.size(); i++) {

				Element jugador = new Element("jugador");

				Element jugador_nombre = new Element("nombre").setText(jugadores.get(i).getName());

				jugador.addContent(jugador_nombre);

				Element puntos = new Element("puntos").setText("" + jugadores.get(i).getPuntos());

				jugador.addContent(puntos);

				Element partidasJugadas = new Element("partidasJugadas")
						.setText("" + jugadores.get(i).getPartidasJugadas());
				jugador.addContent(partidasJugadas);

				doc.getRootElement().addContent(jugador);
			}

			// Salida
			Format format = Format.getPrettyFormat();
			// �, tildes: UTF-8 por ISO-8859-1
			format.setEncoding("UTF-8");
			XMLOutputter xmlOutput = new XMLOutputter(format);
			xmlOutput.output(doc, new FileWriter(ruta));

			// System.out.println("Fichero Guardado! en " + ruta);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
