package utils;

import java.util.ArrayList;
import java.util.Scanner;

import main.Main;
import model.Question;

public class IOMenu {

	private IOMenu(){
		
	}
	/*
	public static String validarString(String loquesea){
	    String original = "��������������u������������������ǿ!�";
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC???";
	    String salida = loquesea;
	    for (int i=0; i<original.length(); i++) {
	    	salida = salida.replace(original.charAt(i), ascii.charAt(i));
	    }
	    return salida;
	}
	*/
	public static int obtener_op() {
		int numero = -1;
		boolean respuestaCorrecta;
		System.out.println("----------------------------------------------------");
		do {
			try {
				do {
					System.out.println(
							MenuEnum.JUGAR.getOPCION_VALOR_MENU() + ":" + MenuEnum.JUGAR.getOPCION_NOMBRE_MENU()
									+ " - " + MenuEnum.ANNADIR.getOPCION_VALOR_MENU() + ":"
									+ MenuEnum.ANNADIR.getOPCION_NOMBRE_MENU() + " - "
									+ MenuEnum.INSTRUCCIONES.getOPCION_VALOR_MENU() + ":"
									+ MenuEnum.INSTRUCCIONES.getOPCION_NOMBRE_MENU() + " - "
									+ MenuEnum.RANKING.getOPCION_VALOR_MENU() + ":"
									+ MenuEnum.RANKING.getOPCION_NOMBRE_MENU() + " - "
									+ MenuEnum.SALIR.getOPCION_VALOR_MENU() + ":"
									+ MenuEnum.SALIR.getOPCION_NOMBRE_MENU());
					System.out.print("Introduce una opci�n: ");
					numero = new Scanner(System.in).nextInt();
					if (numero != MenuEnum.JUGAR.getOPCION_VALOR_MENU()
							&& numero != MenuEnum.ANNADIR.getOPCION_VALOR_MENU()
							&& numero != MenuEnum.INSTRUCCIONES.getOPCION_VALOR_MENU()
							&& numero != MenuEnum.RANKING.getOPCION_VALOR_MENU()
							&& numero != MenuEnum.SALIR.getOPCION_VALOR_MENU()) {
						System.out.println("\nIntroduce un valor correcto\n");
					}
				} while (numero != MenuEnum.JUGAR.getOPCION_VALOR_MENU()
						&& numero != MenuEnum.ANNADIR.getOPCION_VALOR_MENU()
						&& numero != MenuEnum.RANKING.getOPCION_VALOR_MENU()
						&& numero != MenuEnum.INSTRUCCIONES.getOPCION_VALOR_MENU()
						&& numero != MenuEnum.SALIR.getOPCION_VALOR_MENU());
				respuestaCorrecta = true;
			} catch (final Exception e) {
				respuestaCorrecta = false;
				System.out.println("\nIntroduce un valor correcto\n");
			}
		} while (!respuestaCorrecta);
		return numero;
	}

	public static boolean obtener_repetir() {
		int numero = -1;
		boolean respuestaCorrecta;
		boolean preguntar = false;
		System.out.println("----------------------------------------------------");
		do {
			try {
				do {
					System.out.print("�Quieres repetir?: " + AnswerEnum.SI.getOPCION_VALOR_RESPUESTA() + ":"
							+ AnswerEnum.SI.getOPCION_NOMBRE_RESPUESTA() + " - "
							+ AnswerEnum.NO.getOPCION_VALOR_RESPUESTA() + ":"
							+ AnswerEnum.NO.getOPCION_NOMBRE_RESPUESTA() + " ?: ");
					numero = new Scanner(System.in).nextInt();
					if (numero != AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()
							&& numero != AnswerEnum.NO.getOPCION_VALOR_RESPUESTA()) {
						System.out.println("\nIntroduce un valor correcto\n");
					}
					if (numero == AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()) {
						preguntar = true;
					}
				} while (numero != AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()
						&& numero != AnswerEnum.NO.getOPCION_VALOR_RESPUESTA());
				respuestaCorrecta = true;
			} catch (final Exception e) {
				respuestaCorrecta = false;
				System.out.println("\nIntroduce un valor correcto\n");
			}
		} while (!respuestaCorrecta);
		return preguntar;
	}

	public static boolean obtener_pregunta() {
		int numero = -1;
		boolean respuestaCorrecta;
		boolean preguntar = false;
		System.out.println("----------------------------------------------------");
		do {
			try {
				do {
					System.out
							.print("�Quieres a�adir una pregunta?: " + AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()
									+ ":" + AnswerEnum.SI.getOPCION_NOMBRE_RESPUESTA() + " - "
									+ AnswerEnum.NO.getOPCION_VALOR_RESPUESTA() + ":"
									+ AnswerEnum.NO.getOPCION_NOMBRE_RESPUESTA() + " ?: ");
					numero = new Scanner(System.in).nextInt();
					if (numero != AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()
							&& numero != AnswerEnum.NO.getOPCION_VALOR_RESPUESTA()) {
						System.out.println("\nIntroduce un valor correcto\n");
					}
					if (numero == AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()) {
						preguntar = true;
					}
				} while (numero != AnswerEnum.SI.getOPCION_VALOR_RESPUESTA()
						&& numero != AnswerEnum.NO.getOPCION_VALOR_RESPUESTA());
				respuestaCorrecta = true;
			} catch (final Exception e) {
				respuestaCorrecta = false;
				System.out.println("\nIntroduce un valor correcto\n");
			}
		} while (!respuestaCorrecta);
		return preguntar;
	}

	public static int obtener_correcta(String[] soluciones) {
		int numero = -1;
		boolean respuestaCorrecta;
		System.out.println("----------------------------------------------------");
		do {
			try {
				do {
					System.out.print("�Cu�l es la correcta? [" + 0 + "-" + (soluciones.length - 1) + "]: ");
					numero = new Scanner(System.in).nextInt();
					if (numero < 0 || numero > soluciones.length - 1) {
						System.out.println("\nIntroduce un valor correcto\n");
					}
				} while (numero < 0 || numero > soluciones.length - 1);
				respuestaCorrecta = true;
			} catch (final Exception e) {
				respuestaCorrecta = false;
				System.out.println("\nIntroduce un valor correcto\n");
			}
		} while (!respuestaCorrecta);
		return numero;
	}

	public static int obtener_importar() {
		int numero = -1;
		boolean respuestaCorrecta;
		System.out.println("----------------------------------------------------");
		do {
			try {
				do {
					System.out.println(ImportEnum.BUSCAR.getOPCION_VALOR_IMPORTAR() + ":"
							+ ImportEnum.BUSCAR.getOPCION_NOMBRE_IMPORTAR() + " - "
							+ ImportEnum.ANNADIR.getOPCION_VALOR_IMPORTAR() + ":"
							+ ImportEnum.ANNADIR.getOPCION_NOMBRE_IMPORTAR() + " - "
							+ ImportEnum.ATRAS.getOPCION_VALOR_IMPORTAR() + ":"
							+ ImportEnum.ATRAS.getOPCION_NOMBRE_IMPORTAR());

					System.out.print("Introduce una opci�n: ");
					numero = new Scanner(System.in).nextInt();
					if (numero != ImportEnum.BUSCAR.getOPCION_VALOR_IMPORTAR()
							&& numero != ImportEnum.ANNADIR.getOPCION_VALOR_IMPORTAR()
							&& numero != ImportEnum.ATRAS.getOPCION_VALOR_IMPORTAR()) {
						System.out.println("\nIntroduce un valor correcto\n");
					}
				} while (numero != ImportEnum.BUSCAR.getOPCION_VALOR_IMPORTAR()
						&& numero != ImportEnum.ANNADIR.getOPCION_VALOR_IMPORTAR()
						&& numero != ImportEnum.ATRAS.getOPCION_VALOR_IMPORTAR());
				respuestaCorrecta = true;
			} catch (final Exception e) {
				respuestaCorrecta = false;
				System.out.println("\nIntroduce un valor correcto\n");
			}
		} while (!respuestaCorrecta);
		return numero;
	}
}
