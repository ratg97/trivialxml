package model;

public class Question {

	private String pregunta;
	private String[] soluciones;
	private String correcta;
	private String dificultad;
	
	public Question(String pregunta, String[] soluciones, String correcta, String dificultad) {
		super();
		this.pregunta = pregunta;
		this.soluciones = soluciones;
		this.correcta = correcta;
		this.dificultad = dificultad;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String[] getSoluciones() {
		return soluciones;
	}

	public void setSoluciones(String[] soluciones) {
		this.soluciones = soluciones;
	}

	public String getCorrecta() {
		return correcta;
	}

	public void setCorrecta(String correcta) {
		this.correcta = correcta;
	}

	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}
	public void mostrarSoluciones(){
		for (int i =0;i<this.soluciones.length;i++){
			System.out.println("["+i+"] "+this.soluciones[i]);
		}
	}

	
	
}
