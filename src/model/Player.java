package model;

public class Player{
	private String name;
	private int puntos;
	private int partidasJugadas;

	public Player(String name) {
		super();
		this.name = name;
		this.puntos = 0;
		this.partidasJugadas=0;
	}
	

	public Player(String name, int puntos, int partidasJugadas) {
		super();
		this.name = name;
		this.puntos = puntos;
		this.partidasJugadas = partidasJugadas;
	}
	
    public Player(final Player j) {
        this.name = j.name;
        this.puntos = j.puntos;
        this.partidasJugadas = j.partidasJugadas;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPuntos() {
		return puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public int getPartidasJugadas() {
		return partidasJugadas;
	}

	public void setPartidasJugadas(int partidasJugadas) {
		this.partidasJugadas = partidasJugadas;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", puntos=" + puntos + ", partidasJugadas=" + partidasJugadas + "]";
	}
}
