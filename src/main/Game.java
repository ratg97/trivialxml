package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import model.Player;
import model.Question;
import utils.ReadPlayersXML;
import utils.IOMenu;
import utils.WritePlayersXML;

public class Game {

    private Game() {

    }

    public static void annadirPregunta(ArrayList<Question> questions) {
        boolean preguntarPregunta = true;

        while (preguntarPregunta) {

            System.out.print("\nIntroduce la frase: ");
            String pregunta = new Scanner(System.in).nextLine().trim();
            // Quitamos cosas raras...
            //pregunta = IOMenu.validarString(pregunta);

            String dificultad = Main.dificultadArray[getDificultadJuegoAnnadir()];

            String[] soluciones = new String[3];

            System.out.print("\nIntroduce una soluci�n 1: ");
            soluciones[0] = new Scanner(System.in).nextLine().trim();
            // Quitamos cosas raras...
            //soluciones[0] = IOMenu.validarString(soluciones[0]);
            boolean noIgual = true;
            do {
                System.out.print("\nIntroduce otra soluci�n 2: ");
                soluciones[1] = new Scanner(System.in).nextLine().trim();
                // Quitamos cosas raras...
                //soluciones[1] = IOMenu.validarString(soluciones[1]);
                if (soluciones[0].equalsIgnoreCase(soluciones[1])) {
                    System.out.print("\nDetectada coincidencia con la primera pregunta. Repetimos la pregunta...\n");
                    noIgual = false;
                } else {
                    noIgual = true;
                }
            } while (!noIgual);
            boolean noIgual2 = true;
            do {
                System.out.print("\nIntroduce otra soluci�n 3: ");
                soluciones[2] = new Scanner(System.in).nextLine().trim();
                // Quitamos cosas raras...
                //soluciones[2] = IOMenu.validarString(soluciones[2]);
                if (soluciones[2].equalsIgnoreCase(soluciones[0])) {
                    System.out.print("\nDetectada coincidencia con la primera pregunta. Repetimos la pregunta...\n");
                    noIgual2 = false;
                } else {
                    noIgual2 = true;
                }

                if (soluciones[2].equalsIgnoreCase(soluciones[1])) {
                    System.out.print("\nDetectada coincidencia con la segunda pregunta. Repetimos la pregunta...\n");
                    noIgual2 = false;
                } else {
                    noIgual2 = true;
                }
            } while (!noIgual2);

            System.out.println("\n" + pregunta + "\n" + soluciones[0] + "\n" + soluciones[1] + "\n" + soluciones[2]);
            int correcta = IOMenu.obtener_correcta(soluciones);

            questions.add(new Question(pregunta, soluciones, soluciones[correcta], dificultad));

            preguntarPregunta = IOMenu.obtener_pregunta();
        }

    }

    public static void mostrarDificultadArray() {
        for (int i = 0; i < Main.dificultadArray.length; i++) {
            // Visualizaci�n bonita
            if (i == Main.dificultadArray.length - 1) {
                System.out.print(i + ":" + Main.dificultadArray[i]);
            } else {
                System.out.print(i + ":" + Main.dificultadArray[i] + " - ");
            }
        }
    }

    public static int getDificultadJuegoAnnadir() {
        int numero = -1;
        boolean respuestaCorrecta;

        do {
            try {
                do {
                    mostrarDificultadArray();
                    System.out.print("\nElige su dificultad: [" + 0 + "-" + (Main.dificultadArray.length - 1) + "]: ");
                    numero = new Scanner(System.in).nextInt();
                    if (numero < 0 || numero > Main.dificultadArray.length - 1) {
                        System.out.println("\nIntroduce un valor correcto\n");
                    }
                } while (numero < 0 || numero > Main.dificultadArray.length - 1);
                respuestaCorrecta = true;
            } catch (final Exception e) {
                respuestaCorrecta = false;
                System.out.println("\nIntroduce un valor correcto\n");
            }
        } while (!respuestaCorrecta);
        return numero;
    }

    public static int getDificultadJuego() {
        int numero = -1;
        boolean respuestaCorrecta;
        System.out.println("----------------------------------------------------");
        do {
            try {
                do {
                    System.out.print("Modos: ");
                    mostrarDificultadArray();
                    System.out.print("\nElige dificultad: [" + 0 + "-" + (Main.dificultadArray.length - 1) + "]: ");
                    numero = new Scanner(System.in).nextInt();
                    if (numero < 0 || numero > Main.dificultadArray.length - 1) {
                        System.out.println("\nIntroduce un valor correcto\n");
                    }
                } while (numero < 0 || numero > Main.dificultadArray.length - 1);
                respuestaCorrecta = true;
            } catch (final Exception e) {
                respuestaCorrecta = false;
                System.out.println("\nIntroduce un valor correcto\n");
            }
        } while (!respuestaCorrecta);
        return numero;
    }

    public static int getPuntosDificultadJuego(int n) {
        int p = 0;
        switch (n) {
            case 0:
                p = 1;
                break;
            case 1:
                p = 2;
                break;
            case 2:
                p = 3;
                break;
        }
        return p;

    }

    public static void algoritmoJuego(String rutaJug, ArrayList<Question> questions, Player j,
            ArrayList<Player> jugadores) {
        int respuesta = -1;

        int puntos = 0;
        int dificultad = getDificultadJuego();
        int puntosDificultad = getPuntosDificultadJuego(dificultad);
        // Solo queremos las de esa dificultad
        ArrayList<Question> preguntasNivel = new ArrayList<Question>();
        for (int y = 0; y < questions.size(); y++) {
            if (questions.get(y).getDificultad().equals(Main.dificultadArray[dificultad])) {
                preguntasNivel.add(questions.get(y));
            }
        }
        if (!preguntasNivel.isEmpty()) {
            int veces = preguntasNivel.size() > 10 ? 10 : preguntasNivel.size();
            for (int i = 0; i < veces; i++) {
                System.out.println("----------------------------------------------------");
                System.out.printf("%-20s%n", "Player: " + j.getName());
                System.out.println("Puntos actuales: " + puntos + " | Preguntas restantes: " + (veces - i));
                System.out.println("\n" + preguntasNivel.get(i).getPregunta() + "\n");
                preguntasNivel.get(i).mostrarSoluciones();
                respuesta = IOMenu.obtener_correcta(preguntasNivel.get(i).getSoluciones());
                if (preguntasNivel.get(i).getSoluciones()[respuesta] == preguntasNivel.get(i).getCorrecta()) {
                    System.out.println("\nCorrecta\n");
                    puntos += puntosDificultad;
                } else {
                    System.out.println("\nIncorrecta, la correcta era: " + preguntasNivel.get(i).getCorrecta() + "\n");
                    puntos -= puntosDificultad;
                }

            }
            System.out.println("----------------------------------------------------");
            System.out.println("\nGracias por jugar :)\n");
            System.out.println(
                    "Puntos obtenidos: " + puntos + " | Puntos m�ximos por partida: " + veces * puntosDificultad);
            // Actualizar datos...
            ReadPlayersXML.modificarJugador(jugadores, j, puntos);
            WritePlayersXML.escribirJugadores(jugadores, rutaJug);
            // Repetir...
            if (IOMenu.obtener_repetir()) {
            	randomAllQuestions(questions);
                algoritmoJuego(rutaJug, questions, j, jugadores);
            }
        } else {
            System.out.println("# No hay preguntas disponibles a esa dificultad... regresando al men�");
        }
    }
    
    public static void randomAllQuestions(ArrayList<Question> questions){
    	// Revolvemos las tarjetas aleatoriamente
		long seed = System.nanoTime();
		Collections.shuffle(questions, new Random(seed));
		// Adem�s las 3 respuestas tambi�n
        for (int i =0; i<questions.size();i++){
        	Collections.shuffle(Arrays.asList(questions.get(i).getSoluciones()), new Random(seed));
        }
    }
}
