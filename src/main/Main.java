package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.Player;
import model.Question;
import utils.SortPlayers;
import utils.ImportEnum;
import utils.MenuEnum;
import utils.ReadPlayersXML;
import utils.ReadQuestionsXML;
import utils.IOMenu;
import utils.WriteQuestionsXML;

public class Main {

	public static final String[] dificultadArray = { "facil", "media", "dificil" };

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		if (args.length == 1 && args[0].equals("-h")) {
			System.out.println(
					"Trivial con sistema de dificultad y puntos desarrollado por Ra�l Tejedor Garc�a mediante Java y la librer�a JDOM para XML");
		} else {
			String ruta = "trivial.xml";
			String rutaJug = "datos.xml";
			ArrayList<Question> questions = new ArrayList<Question>();
			ArrayList<Player> jugadores = new ArrayList<Player>();

			ReadQuestionsXML.leer(questions, ruta);
			ReadPlayersXML.leerJugador(jugadores, rutaJug);

			int op;
			Player j = null;
			do {
				op = IOMenu.obtener_op();
				if (op == MenuEnum.JUGAR.getOPCION_VALOR_MENU()) {

					if (!questions.isEmpty()) {
						String nick;
						boolean repet;
						System.out.println("----------------------------------------------------");
						System.out.println("Jugadores que ya han jugado:");
						int y = 0;
						for (int i = 0; i < jugadores.size(); i++) {
							if (!(y == 2)) {
								System.out.printf("%-20s", jugadores.get(i).getName());
								y++;
							} else {
								System.out.printf("%-20s%n", jugadores.get(i).getName());
								y = 0;
							}

						}
						System.out.println("\n----------------------------------------------------");
						System.out.println("Introduce el nick del jugador:");
						nick = new Scanner(System.in).nextLine().trim();
						if (nick.length() > 10) {
							System.out.println("Nombre recortado a 10 posiciones...");
							nick = nick.substring(0, 10);
						}
						/*
						// Quitamos cosas raras...
						nick = IOMenu.validarString(nick);
						*/

						repet = ReadPlayersXML.leerJugadorNombre(jugadores, nick);
						// System.out.println(repet);

						if (repet) {
							j = new Player(ReadPlayersXML.obtenerJugador(jugadores, nick));
							System.out.println("Cargando informaci�n del jugador...: " + j.getName());
							System.out.println("Puntos totales: " + j.getPuntos() + " | Partidas jugadas: "
									+ j.getPartidasJugadas());
						} else {
							j = new Player(nick);
							jugadores.add(j);
							// WritePlayersXML.escribirJugadores(jugadores,
							// rutaJug);
						}
						Game.randomAllQuestions(questions);
						Game.algoritmoJuego(rutaJug, questions, j, jugadores);
					} else {
						System.out.println("\nNo se han encontrado tarjetas para jugar...\n");
						int opImportar = IOMenu.obtener_importar();
						if (opImportar != ImportEnum.ATRAS.getOPCION_VALOR_IMPORTAR()) {
							if (opImportar == ImportEnum.BUSCAR.getOPCION_VALOR_IMPORTAR()) {
								// System.out.println("Antes de leer: " +
								// preguntas.size());
								System.out.print("Introduce la ruta a leer: ");
								String rutaImportar = new Scanner(System.in).nextLine().trim();
								ArrayList<Question> auxPreguntas = new ArrayList<Question>();
								ReadQuestionsXML.leer(auxPreguntas, rutaImportar);
								if (!auxPreguntas.isEmpty()) {
									questions.clear();
									for (int i = 0; i < auxPreguntas.size(); i++) {
										questions.add(auxPreguntas.get(i));
									}
									auxPreguntas.clear();
									auxPreguntas = null;
								}
							}

							if (opImportar == ImportEnum.ANNADIR.getOPCION_VALOR_IMPORTAR()) {
								Game.annadirPregunta(questions);
								WriteQuestionsXML.escribir(questions, ruta);
							}
						}
					}

				} else if (op == MenuEnum.ANNADIR.getOPCION_VALOR_MENU()) {
					Game.annadirPregunta(questions);
					System.out.println("# Question a�adida con �xito");
					WriteQuestionsXML.escribir(questions, ruta);

				} else if (op == MenuEnum.INSTRUCCIONES.getOPCION_VALOR_MENU()) {
					System.out.println("\n" + MenuEnum.JUGAR.getOPCION_VALOR_MENU() + ":"
							+ MenuEnum.JUGAR.getOPCION_NOMBRE_MENU() + "\n"
							+ "Precondiciones: Si hay m�s de 10 preguntas en en el XML cargar�n s�lo 10, si hubiese menos se cargar�a ese n�mero determinado\n"
							+ "Jugabilidad: El jugador tendr� que elegir 1 de 3 respuestas posibles a cada pregunta\n"
							+ "Sistema de puntos: seg�n la dificultad escogida:\n" + "facil: Si se acierta: +1\n"
							+ "       Si se falla: -1\n" + "media: Si se acierta: +2\n" + "       Si se falla: -2\n"
							+ "dificil: Si se acierta: +3\n" + "       Si se falla: -3\n"
							+ "Tras acabar la partida se guardar� la informaci�n del jugador"
							+ "\nSi abadona durante la partida no se guardar� nada...");

					System.out.println("\n" + MenuEnum.ANNADIR.getOPCION_VALOR_MENU() + ":"
							+ MenuEnum.ANNADIR.getOPCION_NOMBRE_MENU() + "\n"
							+ "Se da la opci�n de a�adir una pregunta con su dificultad, soluciones y la correcta que el usuario quiera\n"
							+ "CUIDADO!: no se permite �,�,� y otros s�mbolos especiales\n");

					System.out.println("\n" + MenuEnum.RANKING.getOPCION_VALOR_MENU() + ":"
							+ MenuEnum.RANKING.getOPCION_NOMBRE_MENU() + "\n"
							+ "Se visualizar� un listado de los jugadores por nivel de puntos, partidas jugadas, nombre y en su defecto su longitud,\nque han iniciado alguna vez la partida con sus respectivos puntos,\nadem�s se indicar� las partidas jugadas para llegar a ese puntaje\n");

				} else if (op == MenuEnum.RANKING.getOPCION_VALOR_MENU()) {

					if (!jugadores.isEmpty()) {
						System.out.println("----------------------------------------------------");
						System.out.println("Lista de jugadores del trivial:\n");
						System.out.printf("%-20s%-20s%s%n%n", "Nombre", "Puntos", "Partidas jugadas");
						SortPlayers.ordenarPorTodo(jugadores);
						for (int i = 0; i < jugadores.size(); i++) {
							System.out.printf("%-20s%-20s%s%n", jugadores.get(i).getName(),
									jugadores.get(i).getPuntos(), jugadores.get(i).getPartidasJugadas());
						}
					} else {
						System.out.println("No hay ranking a�n, nadie ha jugado");
					}
				}
			} while (op != MenuEnum.SALIR.getOPCION_VALOR_MENU());
			WriteQuestionsXML.escribir(questions, ruta);
		}
	}

}
