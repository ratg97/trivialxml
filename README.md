# **Proyecto de Eclipse** #

El juego del **'Trivial'** para el idioma *Español*.
Recoge de un XML las x preguntas y una vez completadas, los resultados y el nombre del jugador se guardan en otro XML.

![Captura5.PNG](https://bitbucket.org/repo/akkbe59/images/1579706561-Captura5.PNG)
![Captura6.PNG](https://bitbucket.org/repo/akkbe59/images/2761521553-Captura6.PNG)
![Captura7.PNG](https://bitbucket.org/repo/akkbe59/images/2017435190-Captura7.PNG)

# **Metodología:** #

1. No Swing

1. No MVC

1. Todo aleatorio

1. No Constantes. Si Enum

1. No expresiones regulares

1. Librería XML: jdom-2.0.6.jar